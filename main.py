from flask import Flask,render_template,redirect,url_for,request,send_file
import zipfile
import uuid
import os
import io

app = Flask(__name__) 
BASE_DIR = os.path.dirname(os.path.abspath(__file__))

#routes
@app.route("/")
def hello_world():
    return render_template("index.html")
    


@app.route("/convert/",methods=["POST","GET"])
def convert():
    if request.method=="POST":
        uploaded_file = request.files["latexFile"]
        input_file = str(uuid.uuid1())+".zip"
        uploaded_file.save(input_file)
        k= extractAndConvert(input_file)
        zip_io= io.BytesIO(open("static/"+k+".zip",'rb').read())
        #zip_data =zipfile.ZipFile(zip_io,'r')
        os.remove("static/"+k+".zip")
        return send_file(zip_io,mimetype="application/zip")
    else:
        return redirect(url_for('hello_world'))
    
    



def extractAndConvert(extractpath):
    file_id=str(uuid.uuid1())
    output_file = file_id
    with zipfile.ZipFile(extractpath,'r') as zp:
        zp.extractall(output_file)

    os.system('''
    latexmlc {}/main.tex --dest={}/index.html --javascript="https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-mml-chtml.js?config=MML_HTMLorMML" '''.format(output_file,output_file))

    kin = open(output_file+'/ltx-article.css','r')
    css = kin.read()
    kin.close()

    customCSS = open('custom.css','r').read()
    css=customCSS+css

    kout= open(output_file+'/ltx-article.css','w')
    kout.write(css)
    kout.close()

    file_in= open(output_file+'/index.html','r')
    file_content = file_in.read()
    split1,split2=file_content.split("</body>")

    custom_script= '''
            <script>
            var ele = document.getElementsByClassName("ltx_page_main")[0];
            var imgElement = document.createElement("img");
            imgElement.setAttribute("src", "headerImage.png");
            imgElement.setAttribute("alt", "banner");
            imgElement.setAttribute(
                "style",
                "width:100% !important;min-height:auto;"
            );
            ele.insertBefore(imgElement, ele.firstChild);
            </script>
            </body>
    '''
    html = split1+custom_script+split2
    file_in.close()
    file_out = open(output_file+'/index.html','w')
    file_out.write(html)
    file_out.close()

    os.system('''zip -r ./static/{}.zip {}'''.format(output_file,output_file))
    os.system("rm -rf {}".format(file_id))
    os.remove(extractpath)
    
    return file_id


